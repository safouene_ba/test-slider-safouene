import styled from 'styled-components'


const Input = styled.input`
    width: 320px;
    height: 27px;
    background-color: white;
    display: inline-block;
    outline: none;
    padding-left: 10px;
    margin-top : 8px;
    border-radius : 5px;
    border : 1px solid #C8C7C8;
    font: 400 12px 'Source Sans Pro', 'sans-serif';
    &:hover {
    color : #FF9000;
    border : 1px solid #FF9000;
    }
    ::placeholder,
    ::-webkit-input-placeholder {
      color: #C8C7C8;
      font-size : 12px;
    }
    :-ms-input-placeholder {    
      color: #C8C7C8;
      font-size : 12px;
  }
 `

const Label = styled.div`
    width: 320px;
    height: 27px;
    color: ${props => props.selected ? "white" : "black"};
    background-color : ${props => props.selected ? "#FF9000" : "white"};
    border : 1px solid #C8C7C8;
    display: flex;
    justify-content : center;
    align-items : center;
    padding-left: 10px; 
    margin-top : 8px;
    border-radius : 5px;
    font: 400 12px 'Source Sans Pro', 'sans-serif';
    &:hover {
    color : ${props => props.selected ? "white" : "#FF9000"};;
    border : 1px solid #FF9000;
    }
  }
 `

export  { Label, Input }