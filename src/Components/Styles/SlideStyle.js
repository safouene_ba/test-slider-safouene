import styled from 'styled-components'


const Slide = styled.div`
     display: flex;
     align-items: center;
     height : 400px;
     width : 700px;
 `

const ButtonSectionLeft = styled.div`
      display: flex;
      justify-content: flex-end;
      height :60%;
      width : 100%;
  `

const ButtonSectionRight = styled(ButtonSectionLeft)`
  justify-content: flex-start;
`

export { Slide, ButtonSectionLeft, ButtonSectionRight }