import styled from 'styled-components'


const Button = styled.div`
     height : 42px;
     width : 42px;
     border-radius : 30px;
     background-color:   #C8C7C8;
     border : 2px solid  #C8C7C8;
     display : flex;
     align-items: center;
     justify-content: center;
      &:hover {
         border : 2px solid #FF9000;
         background-color : #FF9000;
    }   
 `

export { Button }