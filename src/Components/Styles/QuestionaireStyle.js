import styled from 'styled-components'


const Questionaire = styled.div`
     display: flex;
     flex-direction : column;
     justify-content: flex-start;
     padding-top : 80px;
     height : 400px;
     width : 55 0px;
 `
const Title = styled.div`
    color: black;
    text-align: justify;
    font: 600 14px 'Source Sans Pro', 'sans-serif';
`
const Error = styled.div`
    color: #ED6955;
    text-align: right;
    margin-top : 2px;
    font: 500 8px 'Source Sans Pro', 'sans-serif';
`

export { Questionaire, Title, Error }