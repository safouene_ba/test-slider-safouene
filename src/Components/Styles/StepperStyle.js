import styled from 'styled-components'


const Stepper = styled.div`
    height : 50px;
    display : flex;
    align-items: center;
    justify-content: center;
`

const Barre = styled.div`
    width : 100%;
    height : 3px;
    background-color: ${props => props.background};
`

const Circle = styled.div`
    border: 2px solid ${props => props.background};
    width : 12px;
    height : 12px;
    border-radius : 17px;
 `


const Step = styled.div`
    display : inherit;
    flex : 1;
    flex-direction : column;
    flex : 1;
  `

const Title = styled.div`
    display : inherit;
    flex : 1;
    align-items: center;
    justify-content: center;
    font: 600 14px 'Source Sans Pro', 'sans-serif';
  `



const Timeline = styled.div`
    display : inherit;
    flex : 2;
    align-items: center;
    justify-content: center;
`
export {
    Stepper, Step, Barre, Circle, Title, Timeline
}
