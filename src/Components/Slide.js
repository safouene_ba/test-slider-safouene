import React from 'react';
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { Slide, ButtonSectionLeft, ButtonSectionRight } from './Styles/SlideStyle'
import Button from './Button'
import Questionaire from './Questionaire'
const NEXT = 'next'
const PREVIOUS = 'previous'


class SlideComponent extends React.Component {

  constructor(props) {
    super(props);
    this.state = { error: false, value: null };
  }

  previous = () => {
    const { slide } = this.props
    slide(PREVIOUS)
  }

  next = () => {
    const { slide } = this.props
    const { value } = this.state

    if (value) {
      this.setState({ value, error: false })
      slide(NEXT)
    } else {
      this.setState({ error: true })
    }
  }

  onChange = (value) => {
    this.setState({ value })
  }

  clear = () => {
    this.setState({ value: null })
  }


  render() {
    const { error, value } = this.state;
    const { slideData, position, lastElementPosition } = this.props;
    const showPrevious = position !== 0
    const showNext = position !== lastElementPosition
    return (
      <Slide >
        <ButtonSectionRight>
          {
            showPrevious && <Button onClick={this.previous}>
              <FontAwesomeIcon color="white" icon={['fas', 'arrow-left']} />
            </Button>
          }
        </ButtonSectionRight>
        <Questionaire
          clear={this.clear}
          error={error}
          onChange={this.onChange}
          slideData={slideData}
          selected={value} />
        <ButtonSectionLeft>
          {
            showNext && <Button onClick={this.next}>
              <FontAwesomeIcon color="white" icon={['fas', 'arrow-right']} />
            </Button>
          }
        </ButtonSectionLeft>
      </Slide>
    )
  }
}

SlideComponent.propTypes = {
  position: PropTypes.number,
  slideData: PropTypes.object,
  slide: PropTypes.func
};

SlideComponent.defaultProps = {
  position: 0,
  slideData: {},
  slide: () => { }
};

export default SlideComponent;