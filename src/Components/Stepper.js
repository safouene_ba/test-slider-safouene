import React from 'react';
import PropTypes from 'prop-types';
import {
    Stepper, Step, Barre, Circle, Title, Timeline
} from './Styles/StepperStyle'




function StepComponent({ position, title }) {
  return title.map((item, index) => {
    const background = position === index ? '#FF9000' : '#C8C7C8';
    return (
      <Step key={item}>
        <Title background>{item}</Title>
        <Timeline>
          <Barre background={background} />
          <Circle background={background} />
        </Timeline>
      </Step>
    )
  }
  )
}

function StepperComponent(props) {
  return (
    <Stepper>
      {StepComponent(props)}
    </Stepper>
  );
}


Stepper.propTypes = {
  position: PropTypes.number,
  title: PropTypes.array
};

Stepper.defaultProps = {
  position: 0,
  title: []
};

export default StepperComponent;