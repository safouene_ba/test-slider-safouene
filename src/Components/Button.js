import React from 'react';
import {Button} from './Styles/ButtonStyle'
 



class ButtonComponent extends React.Component {

  render() {
    const { onClick, checked } = this.props
    return (
      <Button checked={checked} onClick={onClick} >
        {this.props.children}
      </Button>
    )
  }
}



export default ButtonComponent;