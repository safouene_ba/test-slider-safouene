import React from 'react';
import PropTypes from 'prop-types';
import Input from './Input'
import { Questionaire, Title, Error } from './Styles/QuestionaireStyle'



class QuestionaireComponent extends React.Component {


    onChange = (props) => {
        const { onChange } = this.props;
        onChange(props)
    }


    renderInputs = () => {
        const { slideData, error, selected } = this.props

        if (slideData.type === 'select') {
            return slideData.data.map((item) => <Input
                key={item.value}
                error={error}
                type={slideData.type}
                inputData={item}
                onChange={this.onChange}
                selected={selected}

            />)
        }
        return <Input type={slideData.type} onChange={this.onChange}
            inputData={slideData.label} />;
    }


    render() {
        const { slideData, error } = this.props

        return (
            <Questionaire >
                <Title>{slideData.label}</Title>
                {
                    this.renderInputs()
                }
                {error && <Error>Champ obligatiore</Error>}
            </Questionaire>
        )
    }
}

QuestionaireComponent.propTypes = {
    content: PropTypes.object,
};

QuestionaireComponent.defaultProps = {
    content: null,
};




export default QuestionaireComponent;