import React from 'react';
import { Input, Label } from './Styles/InputStyle'


class InputComponent extends React.Component {

  onChange = (e) => {
    e.preventDefault();
    const { onChange, inputData, type } = this.props
    const value = type === 'select' ? inputData.value : e.target.value
    onChange(value)
  }

  render() {
    const { inputData, type, selected } = this.props
    if (type === 'select') {
      return (
        <Label
          selected={selected === inputData.value}
          onClick={this.onChange}
        >
          {inputData.label}
        </Label>
      )
    }
    return (
      <Input
        selected={selected > 18}
        onChange={this.onChange}
        placeholder={'Ex : 23'}
        type={'number'}
        value={inputData.label} />
    )
  }
}



export default InputComponent;