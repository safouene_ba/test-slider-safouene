import React from 'react';
import styled from 'styled-components'
import Stepper from './Stepper'
import Slide from './Slide'
import apiCall from './Server/Api'
 
const Slider = styled.div`
     margin-top : 30px;
 `
 
class SliderComponent extends React.Component {
  constructor(props) {
    super(props);
     this.state = { position: 0, questionnaireData : null, loading: true , error : null };
   }

   componentDidMount(){ 
     // api call goes here
      apiCall().then(questionnaireData => {
        this.setState({questionnaireData : questionnaireData.data, loading : false  })
      }).catch(error=>{
        this.setState({error : error, loading : false   })
      })
    }

    slide = (direction) => {
       this.setState((state) => {
        if(direction === 'next'){
          return {position: state.position + 1};
        }
        return {position: state.position - 1};

      });
    }

    render() {
      const {questionnaireData, position, loading, error} = this.state;
      
       if(loading){
         return <h2>api call ...</h2>
       }

       if(error){
        return <h2>something is wrong please chack api call</h2>
      }

      const title = questionnaireData ? Object.keys(questionnaireData) : null;
      const slideData = (questionnaireData && title)  ? questionnaireData[title[position]] : null;
      const lastElementPosition = title ? title.length - 1 : null;
 
      return (
        <Slider>
            <Stepper position={position} title={title} />
            <Slide
                title={title}
                position={position}
                slide={this.slide}
                lastElementPosition={lastElementPosition}
                slideData={slideData}
            />
        </Slider>
      )
    }
  }

export default SliderComponent;
