import React from 'react';
import Slider from './Components/Slider' 
import styled from 'styled-components'
import { library } from '@fortawesome/fontawesome-svg-core'
 import { faArrowLeft,faArrowRight, faCheck } from '@fortawesome/free-solid-svg-icons'

library.add(faArrowLeft,faArrowRight, faCheck)

const Container = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
`


function App() {
  return (
    <Container>
     <Slider/>
    </Container>
  );
}

export default App;
